﻿-- Inspired by https://stackoverflow.com/questions/37078170/mimic-string-split-without-custom-function-in-sql-server-2008
CREATE FUNCTION [dbo].[fnSplitReferenceCollectionAsTable] 
(
    @inputString nvarchar(MAX)
)
RETURNS 
@Result TABLE 
(
    Value nvarchar(200)
)
AS
BEGIN
    DECLARE @inputAsXml XML
    SELECT @inputAsXml = CAST('<A>' + REPLACE(@inputString, '|', '</A><A>') + '</A>' AS XML)

    INSERT INTO @Result 
	SELECT [ValueToReturn] FROM
		(SELECT [ValueAsNode].value('.', 'nvarchar(200)') as [ValueToReturn]
		FROM @inputAsXml.nodes('/A') AS inputAsTable([ValueAsNode])) as AllValues
	WHERE LEN([ValueToReturn]) > 0

    RETURN 
END