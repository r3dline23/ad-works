﻿CREATE TABLE [dbo].[Team] (
    [Id]            NVARCHAR (50)  NOT NULL,
    [Name]          NVARCHAR (200) NOT NULL,
    [MemberUserIds] NVARCHAR (MAX) NOT NULL,
    [JSON]          NVARCHAR (MAX) NOT NULL,
    [SpaceId]       NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Team_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_TeamNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Team_SpaceId]
    ON [dbo].[Team]([SpaceId] ASC);

