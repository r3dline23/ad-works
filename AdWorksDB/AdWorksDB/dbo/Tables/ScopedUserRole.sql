﻿CREATE TABLE [dbo].[ScopedUserRole] (
    [Id]         NVARCHAR (50)  NOT NULL,
    [TeamId]     NVARCHAR (50)  NOT NULL,
    [UserRoleId] NVARCHAR (50)  NOT NULL,
    [SpaceId]    NVARCHAR (50)  NULL,
    [JSON]       NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ScopedUserRole_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

