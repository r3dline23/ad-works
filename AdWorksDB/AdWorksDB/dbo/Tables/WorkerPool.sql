﻿CREATE TABLE [dbo].[WorkerPool] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [SortOrder]   INT            NOT NULL,
    [IsDefault]   BIT            NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_WorkerPool_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_WorkerPoolNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_WorkerPool_DataVersion]
    ON [dbo].[WorkerPool]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WorkerPool_SpaceId]
    ON [dbo].[WorkerPool]([SpaceId] ASC);

