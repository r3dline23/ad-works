﻿CREATE TABLE [dbo].[ProjectTrigger] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [ProjectId]   NVARCHAR (50)  NOT NULL,
    [TriggerType] NVARCHAR (50)  NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [IsDisabled]  BIT            DEFAULT ((0)) NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ProjectTrigger_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_ProjectTriggerNameUniquePerSpace] UNIQUE NONCLUSTERED ([ProjectId] ASC, [Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProjectTrigger_Project]
    ON [dbo].[ProjectTrigger]([ProjectId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProjectTrigger_SpaceId]
    ON [dbo].[ProjectTrigger]([SpaceId] ASC);

