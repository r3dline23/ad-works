﻿CREATE TABLE [dbo].[Script0162MigrateTeamsToNewStructure] (
    [Id]              NVARCHAR (50)  NOT NULL,
    [Name]            NVARCHAR (200) NOT NULL,
    [MemberUserIds]   NVARCHAR (MAX) NOT NULL,
    [ProjectIds]      NVARCHAR (MAX) NOT NULL,
    [EnvironmentIds]  NVARCHAR (MAX) NOT NULL,
    [JSON]            NVARCHAR (MAX) NOT NULL,
    [TenantIds]       NVARCHAR (MAX) NULL,
    [TenantTags]      NVARCHAR (MAX) NULL,
    [ProjectGroupIds] NVARCHAR (MAX) NULL
);

