﻿CREATE TABLE [dbo].[Lifecycle] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Lifecycle_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_LifecycleUniqueNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Lifecycle_DataVersion]
    ON [dbo].[Lifecycle]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Lifecycle_SpaceId]
    ON [dbo].[Lifecycle]([SpaceId] ASC);

