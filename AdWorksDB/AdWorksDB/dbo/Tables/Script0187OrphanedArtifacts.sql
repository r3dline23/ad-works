﻿CREATE TABLE [dbo].[Script0187OrphanedArtifacts] (
    [Id]                 NVARCHAR (50)      NOT NULL,
    [Filename]           NVARCHAR (200)     NOT NULL,
    [Created]            DATETIMEOFFSET (7) NOT NULL,
    [ProjectId]          NVARCHAR (50)      NULL,
    [EnvironmentId]      NVARCHAR (50)      NULL,
    [JSON]               NVARCHAR (MAX)     NOT NULL,
    [TenantId]           NVARCHAR (50)      NULL,
    [DataVersion]        VARBINARY (20)     NOT NULL,
    [ServerTaskId]       NVARCHAR (50)      NULL,
    [RelatedDocumentIds] NVARCHAR (MAX)     NULL,
    [SpaceId]            NVARCHAR (50)      NOT NULL
);

