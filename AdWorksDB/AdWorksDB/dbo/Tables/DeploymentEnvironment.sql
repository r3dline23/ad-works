﻿CREATE TABLE [dbo].[DeploymentEnvironment] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [SortOrder]   INT            NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_DeploymentEnvironment_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_DeploymentEnvironmentNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DeploymentEnvironment_DataVersion]
    ON [dbo].[DeploymentEnvironment]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DeploymentEnvironment_SpaceId]
    ON [dbo].[DeploymentEnvironment]([SpaceId] ASC);

