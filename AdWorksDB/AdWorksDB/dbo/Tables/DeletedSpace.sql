﻿CREATE TABLE [dbo].[DeletedSpace] (
    [Id]   NVARCHAR (50)  NOT NULL,
    [Name] NVARCHAR (200) NOT NULL,
    [JSON] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_DeletedSpace_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

