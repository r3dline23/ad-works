﻿CREATE TABLE [dbo].[Tenant] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [ProjectIds]  NVARCHAR (MAX) NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [TenantTags]  NVARCHAR (MAX) NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Tenant_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_TenantNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Tenant_DataVersion]
    ON [dbo].[Tenant]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Tenant_SpaceId]
    ON [dbo].[Tenant]([SpaceId] ASC);

