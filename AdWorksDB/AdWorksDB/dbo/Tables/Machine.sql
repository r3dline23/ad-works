﻿CREATE TABLE [dbo].[Machine] (
    [Id]                 NVARCHAR (50)  NOT NULL,
    [Name]               NVARCHAR (200) NOT NULL,
    [IsDisabled]         BIT            NOT NULL,
    [Roles]              NVARCHAR (MAX) NOT NULL,
    [EnvironmentIds]     NVARCHAR (MAX) NOT NULL,
    [JSON]               NVARCHAR (MAX) NOT NULL,
    [MachinePolicyId]    NVARCHAR (50)  NULL,
    [TenantIds]          NVARCHAR (MAX) NULL,
    [TenantTags]         NVARCHAR (MAX) NULL,
    [Thumbprint]         NVARCHAR (50)  NULL,
    [Fingerprint]        NVARCHAR (50)  NULL,
    [CommunicationStyle] NVARCHAR (50)  NOT NULL,
    [DataVersion]        ROWVERSION     NOT NULL,
    [SpaceId]            NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Machine_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_MachineNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Machine_MachinePolicy]
    ON [dbo].[Machine]([MachinePolicyId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Machine_DataVersion]
    ON [dbo].[Machine]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Machine_SpaceId]
    ON [dbo].[Machine]([SpaceId] ASC);

