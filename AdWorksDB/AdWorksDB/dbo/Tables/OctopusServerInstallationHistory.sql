﻿CREATE TABLE [dbo].[OctopusServerInstallationHistory] (
    [Id]        NVARCHAR (50)      NOT NULL,
    [Node]      NVARCHAR (200)     NULL,
    [Version]   NVARCHAR (200)     NULL,
    [Installed] DATETIMEOFFSET (7) NOT NULL,
    [JSON]      NVARCHAR (MAX)     NOT NULL
);

