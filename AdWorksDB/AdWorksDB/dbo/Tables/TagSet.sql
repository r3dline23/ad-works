﻿CREATE TABLE [dbo].[TagSet] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [SortOrder]   INT            NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_TagSet_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_TagSetNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TagSet_DataVersion]
    ON [dbo].[TagSet]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TagSet_SpaceId]
    ON [dbo].[TagSet]([SpaceId] ASC);

