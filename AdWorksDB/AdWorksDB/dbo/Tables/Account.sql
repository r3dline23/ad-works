﻿CREATE TABLE [dbo].[Account] (
    [Id]             NVARCHAR (210) NOT NULL,
    [AccountType]    NVARCHAR (50)  NOT NULL,
    [Name]           NVARCHAR (200) NOT NULL,
    [EnvironmentIds] NVARCHAR (MAX) NOT NULL,
    [JSON]           NVARCHAR (MAX) NOT NULL,
    [TenantIds]      NVARCHAR (MAX) NULL,
    [TenantTags]     NVARCHAR (MAX) NULL,
    [DataVersion]    ROWVERSION     NOT NULL,
    [SpaceId]        NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Account_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_AccountUniqueNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Account_DataVersion]
    ON [dbo].[Account]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Account_SpaceId]
    ON [dbo].[Account]([SpaceId] ASC);

