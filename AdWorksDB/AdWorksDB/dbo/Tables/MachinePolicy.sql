﻿CREATE TABLE [dbo].[MachinePolicy] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [IsDefault]   BIT            DEFAULT ((0)) NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_MachinePolicy_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_MachinePolicyNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MachinePolicy_DataVersion]
    ON [dbo].[MachinePolicy]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MachinePolicy_SpaceId]
    ON [dbo].[MachinePolicy]([SpaceId] ASC);

