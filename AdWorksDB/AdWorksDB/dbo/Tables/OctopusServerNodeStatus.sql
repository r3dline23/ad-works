﻿CREATE TABLE [dbo].[OctopusServerNodeStatus] (
    [Id]                  NVARCHAR (50)      NOT NULL,
    [OctopusServerNodeId] NVARCHAR (250)     NOT NULL,
    [LastSeen]            DATETIMEOFFSET (7) NOT NULL,
    [Rank]                NVARCHAR (50)      NOT NULL,
    [IsOnline]            BIT                NOT NULL,
    [JSON]                NVARCHAR (MAX)     NOT NULL,
    CONSTRAINT [PK_OctopusServerNodeStatus_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

