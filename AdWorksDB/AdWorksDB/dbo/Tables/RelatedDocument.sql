﻿CREATE TABLE [dbo].[RelatedDocument] (
    [Id]                   NVARCHAR (250) NOT NULL,
    [Table]                VARCHAR (40)   NOT NULL,
    [RelatedDocumentId]    NVARCHAR (250) NOT NULL,
    [RelatedDocumentTable] VARCHAR (40)   NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_RelatedDocument_Id]
    ON [dbo].[RelatedDocument]([Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RelatedDocument_RelatedDocumentId]
    ON [dbo].[RelatedDocument]([RelatedDocumentId] ASC)
    INCLUDE([Id], [Table]);

