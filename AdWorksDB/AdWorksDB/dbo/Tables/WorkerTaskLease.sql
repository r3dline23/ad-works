﻿CREATE TABLE [dbo].[WorkerTaskLease] (
    [Id]        NVARCHAR (50)  NOT NULL,
    [WorkerId]  NVARCHAR (50)  NOT NULL,
    [TaskId]    NVARCHAR (50)  NOT NULL,
    [Exclusive] BIT            DEFAULT ((0)) NOT NULL,
    [Name]      NVARCHAR (200) NOT NULL,
    [JSON]      NVARCHAR (MAX) NOT NULL,
    [SpaceId]   NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_WorkerTaskLease_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_WorkerTaskLease_SpaceId]
    ON [dbo].[WorkerTaskLease]([SpaceId] ASC);

