﻿CREATE TABLE [dbo].[Feed] (
    [Id]          NVARCHAR (210) NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [FeedUri]     NVARCHAR (512) NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [FeedType]    NVARCHAR (50)  NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Feed_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_FeedUniqueNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Feed_DataVersion]
    ON [dbo].[Feed]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Feed_SpaceId]
    ON [dbo].[Feed]([SpaceId] ASC);

