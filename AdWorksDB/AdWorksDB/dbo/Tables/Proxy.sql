﻿CREATE TABLE [dbo].[Proxy] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Proxy_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_ProxyNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Proxy_DataVersion]
    ON [dbo].[Proxy]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Proxy_SpaceId]
    ON [dbo].[Proxy]([SpaceId] ASC);

