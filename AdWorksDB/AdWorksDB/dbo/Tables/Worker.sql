﻿CREATE TABLE [dbo].[Worker] (
    [Id]                 NVARCHAR (50)  NOT NULL,
    [Name]               NVARCHAR (200) NOT NULL,
    [IsDisabled]         BIT            NOT NULL,
    [WorkerPoolIds]      NVARCHAR (MAX) NULL,
    [MachinePolicyId]    NVARCHAR (50)  NOT NULL,
    [Thumbprint]         NVARCHAR (50)  NULL,
    [Fingerprint]        NVARCHAR (50)  NULL,
    [CommunicationStyle] NVARCHAR (50)  NOT NULL,
    [JSON]               NVARCHAR (MAX) NOT NULL,
    [DataVersion]        ROWVERSION     NOT NULL,
    [SpaceId]            NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Worker_Id] PRIMARY KEY NONCLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_WorkerNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_Worker_MachinePolicy]
    ON [dbo].[Worker]([MachinePolicyId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Worker_DataVersion]
    ON [dbo].[Worker]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Worker_SpaceId]
    ON [dbo].[Worker]([SpaceId] ASC);

