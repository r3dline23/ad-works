﻿CREATE TABLE [dbo].[LibraryVariableSet] (
    [Id]            NVARCHAR (50)  NOT NULL,
    [Name]          NVARCHAR (200) NOT NULL,
    [VariableSetId] NVARCHAR (150) NULL,
    [ContentType]   NVARCHAR (50)  NOT NULL,
    [JSON]          NVARCHAR (MAX) NOT NULL,
    [SpaceId]       NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_LibraryVariableSet_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_LibraryVariableSetNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_LibraryVariableSet_SpaceId]
    ON [dbo].[LibraryVariableSet]([SpaceId] ASC);

