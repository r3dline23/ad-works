﻿CREATE TABLE [dbo].[ActionTemplateVersion] (
    [Id]                     NVARCHAR (50)  NOT NULL,
    [Name]                   NVARCHAR (200) NOT NULL,
    [Version]                INT            NOT NULL,
    [LatestActionTemplateId] NVARCHAR (50)  NOT NULL,
    [ActionType]             NVARCHAR (50)  NOT NULL,
    [JSON]                   NVARCHAR (MAX) NOT NULL,
    [SpaceId]                NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ActionTemplateVersion_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_ActionTemplateVersionUniqueNameVersionPerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [Version] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ActionTemplateVersion_LatestActionTemplateId]
    ON [dbo].[ActionTemplateVersion]([LatestActionTemplateId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ActionTemplateVersion_SpaceId]
    ON [dbo].[ActionTemplateVersion]([SpaceId] ASC);

