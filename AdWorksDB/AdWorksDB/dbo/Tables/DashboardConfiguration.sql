﻿CREATE TABLE [dbo].[DashboardConfiguration] (
    [Id]                     NVARCHAR (50)  NOT NULL,
    [IncludedEnvironmentIds] NVARCHAR (MAX) NOT NULL,
    [IncludedProjectIds]     NVARCHAR (MAX) NOT NULL,
    [JSON]                   NVARCHAR (MAX) NOT NULL,
    [IncludedTenantTags]     NVARCHAR (MAX) NULL,
    [IncludedTenantIds]      NVARCHAR (MAX) NULL,
    [SpaceId]                NVARCHAR (50)  NOT NULL,
    [UserId]                 NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_DashboardConfiguration_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DashboardConfiguration_SpaceId_UserId]
    ON [dbo].[DashboardConfiguration]([SpaceId] ASC, [UserId] ASC);

