﻿CREATE TABLE [dbo].[ProjectGroup] (
    [Id]          NVARCHAR (50)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [JSON]        NVARCHAR (MAX) NOT NULL,
    [DataVersion] ROWVERSION     NOT NULL,
    [SpaceId]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ProjectGroup_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_ProjectGroupUniqueNamePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProjectGroup_DataVersion]
    ON [dbo].[ProjectGroup]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProjectGroup_SpaceId]
    ON [dbo].[ProjectGroup]([SpaceId] ASC);

