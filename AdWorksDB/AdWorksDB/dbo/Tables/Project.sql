﻿CREATE TABLE [dbo].[Project] (
    [Id]                            NVARCHAR (50)  NOT NULL,
    [Name]                          NVARCHAR (200) NOT NULL,
    [Slug]                          NVARCHAR (210) NOT NULL,
    [IsDisabled]                    BIT            NOT NULL,
    [VariableSetId]                 NVARCHAR (150) NULL,
    [DeploymentProcessId]           NVARCHAR (50)  NULL,
    [ProjectGroupId]                NVARCHAR (50)  NOT NULL,
    [LifecycleId]                   NVARCHAR (50)  NOT NULL,
    [AutoCreateRelease]             BIT            NOT NULL,
    [JSON]                          NVARCHAR (MAX) NOT NULL,
    [IncludedLibraryVariableSetIds] NVARCHAR (MAX) NULL,
    [DiscreteChannelRelease]        BIT            DEFAULT ((0)) NOT NULL,
    [DataVersion]                   ROWVERSION     NOT NULL,
    [ClonedFromProjectId]           NVARCHAR (50)  NULL,
    [SpaceId]                       NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Project_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_ProjectNameUniquePerSpace] UNIQUE NONCLUSTERED ([Name] ASC, [SpaceId] ASC),
    CONSTRAINT [UQ_ProjectSlugUniquePerSpace] UNIQUE NONCLUSTERED ([Slug] ASC, [SpaceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Project_DiscreteChannelRelease]
    ON [dbo].[Project]([Id] ASC)
    INCLUDE([DiscreteChannelRelease]);


GO
CREATE NONCLUSTERED INDEX [IX_Project_DataVersion]
    ON [dbo].[Project]([DataVersion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Project_ClonedFromProjectId]
    ON [dbo].[Project]([ClonedFromProjectId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Project_SpaceId]
    ON [dbo].[Project]([SpaceId] ASC);

