﻿CREATE TABLE [dbo].[Space] (
    [Id]               NVARCHAR (50)  NOT NULL,
    [Name]             NVARCHAR (20)  NOT NULL,
    [IsDefault]        BIT            NOT NULL,
    [JSON]             NVARCHAR (MAX) NOT NULL,
    [TaskQueueStopped] BIT            NOT NULL,
    CONSTRAINT [PK_Space_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UQ_SpaceNameUnique] UNIQUE NONCLUSTERED ([Name] ASC)
);

