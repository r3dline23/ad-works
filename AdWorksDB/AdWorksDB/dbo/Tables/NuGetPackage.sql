﻿CREATE TABLE [dbo].[NuGetPackage] (
    [Id]              VARCHAR (50)   NOT NULL,
    [PackageId]       NVARCHAR (100) NOT NULL,
    [Version]         NVARCHAR (349) NOT NULL,
    [VersionMajor]    INT            NOT NULL,
    [VersionMinor]    INT            NOT NULL,
    [VersionBuild]    INT            NOT NULL,
    [VersionRevision] INT            NOT NULL,
    [VersionSpecial]  NVARCHAR (250) NULL,
    [JSON]            NVARCHAR (MAX) NOT NULL,
    [SpaceId]         NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_NuGetPackage_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NuGetPackage_SpaceId]
    ON [dbo].[NuGetPackage]([SpaceId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NuGetPackage_PackageId_SpaceId]
    ON [dbo].[NuGetPackage]([PackageId] ASC, [SpaceId] ASC);

