﻿CREATE VIEW [dbo].[Dashboard] AS
SELECT 
	d.Id AS Id,
	d.Created AS Created,
	d.ProjectId AS ProjectId,
	d.EnvironmentId AS EnvironmentId,
	d.ReleaseId AS ReleaseId,
	d.TaskId AS TaskId,
	d.ChannelId AS ChannelId,
	d.SpaceId AS SpaceId,
	CurrentOrPrevious,
	t.[State] AS [State],
	t.HasPendingInterruptions AS HasPendingInterruptions,
	t.HasWarningsOrErrors AS HasWarningsOrErrors,
	t.ErrorMessage AS ErrorMessage,
	t.QueueTime AS QueueTime,
	t.StartTime AS StartTime,
	t.CompletedTime AS CompletedTime,
	r.[Version] AS [Version]
FROM 
(
	SELECT 
		'C' AS CurrentOrPrevious,
		d.Id AS Id,
		d.Created AS Created,
		d.ProjectId AS ProjectId,
		d.EnvironmentId AS EnvironmentId,
		d.ReleaseId AS ReleaseId,
		d.TaskId AS TaskId,
		d.ChannelId AS ChannelId,	
		d.SpaceId AS SpaceId,
		ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId ORDER BY Created DESC) AS [Rank]
	FROM [Deployment] d
	INNER JOIN [ServerTask] t 
	ON t.Id = d.TaskId
		AND t.SpaceId = d.SpaceId
	WHERE NOT ((t.State = 'Canceled' OR t.State = 'Cancelling') AND t.StartTime IS NULL)
	UNION
	SELECT 
		'P' AS CurrentOrPrevious, 
		d.Id AS Id,
		d.Created AS Created,
		d.ProjectId AS ProjectId,
		d.EnvironmentId AS EnvironmentId,
		d.ReleaseId AS ReleaseId,
		d.TaskId AS TaskId,
		d.ChannelId AS ChannelId,
		d.SpaceId AS SpaceId,
		ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId ORDER BY Created DESC) AS [Rank]
	FROM [Deployment] d
	INNER JOIN [ServerTask] t 
	ON t.Id = d.TaskId
		AND t.SpaceId = d.SpaceId
	LEFT HASH JOIN 
	(
		SELECT 
			Id,
			SpaceId
		FROM 
		(
			SELECT 
				d.Id AS Id,
				d.SpaceId AS SpaceId,
				ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId ORDER BY Created DESC) AS [Rank] 
			FROM [Deployment] d
			INNER JOIN [ServerTask] t 
			ON t.Id = d.TaskId
				AND t.SpaceId = d.SpaceId
			WHERE NOT ((t.State = 'Canceled' OR t.State = 'Cancelling') AND t.StartTime IS NULL)
		) AS LatestDeployment 
		WHERE [Rank] = 1
	) AS l
	ON d.Id = l.Id
		AND d.SpaceId = l.SpaceId
	WHERE t.State = 'Success' 
		AND l.Id IS NULL
) AS d 
INNER JOIN [ServerTask] t 
ON t.Id = d.TaskId
	AND t.SpaceId = d.SpaceId
INNER JOIN [Release] r 
ON r.Id = d.ReleaseId
	AND r.SpaceId = d.SpaceId
WHERE ([Rank] = 1 AND CurrentOrPrevious = 'P') OR ([Rank] = 1 AND CurrentOrPrevious = 'C')