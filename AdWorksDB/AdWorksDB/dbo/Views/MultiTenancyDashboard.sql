﻿CREATE VIEW [dbo].[MultiTenancyDashboard] AS
SELECT 
	d.Id AS Id,
	d.Created AS Created,
	d.ProjectId AS ProjectId,
	d.EnvironmentId AS EnvironmentId,
	d.ReleaseId AS ReleaseId,
	d.TenantId AS TenantId,
	d.TaskId AS TaskId,
	d.ChannelId AS ChannelId,
	d.SpaceId AS SpaceId,
	CurrentOrPrevious,
	t.[State] AS [State],
	t.HasPendingInterruptions AS HasPendingInterruptions,
	t.HasWarningsOrErrors AS HasWarningsOrErrors,
	t.ErrorMessage AS ErrorMessage,
	t.QueueTime AS QueueTime,
	t.StartTime AS StartTime,
	t.CompletedTime AS CompletedTime,
	r.[Version] AS [Version]
FROM 
(
	SELECT 
		'C' AS CurrentOrPrevious,
		d.Id AS Id,
		d.Created AS Created,
		d.ProjectId AS ProjectId,
		d.EnvironmentId AS EnvironmentId,
		d.ReleaseId AS ReleaseId,
		d.TenantId AS TenantId,
		d.TaskId AS TaskId,		
		d.ChannelId AS ChannelId,
		d.SpaceId AS SpaceId,
		ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId, d.TenantId ORDER BY Created DESC) AS [Rank]
	FROM [Deployment] d
	INNER JOIN [ServerTask] t 
	ON t.Id = d.TaskId
	WHERE NOT ((t.State = 'Canceled' OR t.State = 'Cancelling') AND t.StartTime IS NULL)
	UNION
	SELECT 
		'P' AS CurrentOrPrevious, 
		d.Id AS Id,
		d.Created AS Created,
		d.ProjectId AS ProjectId,
		d.EnvironmentId AS EnvironmentId,
		d.ReleaseId AS ReleaseId,
		d.TenantId AS TenantId,
		d.TaskId AS TaskId,
		d.ChannelId AS ChannelId,
		d.SpaceId AS SpaceId,
		ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId, d.TenantId ORDER BY Created DESC) AS [Rank]
	FROM [Deployment] d
	INNER JOIN [ServerTask] t 
	ON t.Id = d.TaskId
	WHERE t.State = 'Success' 
		AND d.Id NOT IN 
	(
		SELECT Id 
		FROM 
		(
			SELECT 
				d.Id AS Id, 
				ROW_NUMBER() OVER (PARTITION BY d.EnvironmentId, d.ProjectId, d.TenantId ORDER BY Created DESC) AS [Rank] 
			FROM [Deployment] d
			INNER JOIN [ServerTask] t 
			ON t.Id = d .TaskId
			WHERE NOT ((t.State = 'Canceled' OR t.State = 'Cancelling') AND t.StartTime IS NULL)
		) LatestDeployment 
		WHERE [Rank] = 1
	)
) d 
INNER JOIN [ServerTask] t 
ON t.Id = d.TaskId
INNER JOIN [Release] r 
ON r.Id = d.ReleaseId
WHERE ([Rank] = 1 AND CurrentOrPrevious = 'P') OR ([Rank] = 1 AND CurrentOrPrevious = 'C')