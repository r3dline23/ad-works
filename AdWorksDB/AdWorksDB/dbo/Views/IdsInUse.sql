﻿CREATE VIEW [dbo].[IdsInUse] AS 
  SELECT [Id], [SpaceId], 'Account' AS [Type], [Name] FROM dbo.[Account] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'ActionTemplate' AS [Type], [Name] FROM dbo.[ActionTemplate] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'ActionTemplateVersion' AS [Type], [Name] FROM dbo.[ActionTemplateVersion] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'ApiKey' AS [Type], [ApiKeyHashed] AS [Name] FROM dbo.[ApiKey] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Certificate' AS [Type], [Name] FROM dbo.[Certificate] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Channel' AS [Type], [Name] FROM dbo.[Channel] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'CommunityActionTemplate' AS [Type], [Name] FROM dbo.[CommunityActionTemplate] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'ActivityLogStorageConfiguration, ArtifactStorageConfiguration, AuthenticationConfiguration, BuiltInRepositoryConfiguration, CertificateConfiguration, FeaturesConfiguration, LetsEncryptConfiguration, License, MaintenanceConfiguration, PerformanceConfiguration, ScheduleConfiguration, ServerConfiguration, SmtpConfiguration, UpgradeAvailability, UpgradeConfiguration, WebPortalConfiguration, WebPortalConfigurationForMigration' AS [Type], '' AS [Name] FROM dbo.[Configuration] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'DashboardConfiguration' AS [Type], '' AS [Name] FROM dbo.[DashboardConfiguration] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'DeploymentEnvironment' AS [Type], [Name] FROM dbo.[DeploymentEnvironment] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'DeploymentProcess' AS [Type], '' AS [Name] FROM dbo.[DeploymentProcess] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Feed' AS [Type], [Name] FROM dbo.[Feed] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Interruption' AS [Type], '' AS [Name] FROM dbo.[Interruption] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'Invitation' AS [Type], '' AS [Name] FROM dbo.[Invitation] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'LibraryVariableSet' AS [Type], [Name] FROM dbo.[LibraryVariableSet] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Lifecycle' AS [Type], [Name] FROM dbo.[Lifecycle] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'DeploymentTarget' AS [Type], [Name] FROM dbo.[Machine] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'MachinePolicy' AS [Type], [Name] FROM dbo.[MachinePolicy] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'IndexedPackage' AS [Type], '' AS [Name] FROM dbo.[NuGetPackage] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'OctopusServerNode' AS [Type], [Name] FROM dbo.[OctopusServerNode] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Project' AS [Type], [Name] FROM dbo.[Project] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'ProjectGroup' AS [Type], [Name] FROM dbo.[ProjectGroup] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'ProjectTrigger' AS [Type], [Name] FROM dbo.[ProjectTrigger] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Proxy' AS [Type], [Name] FROM dbo.[Proxy] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Release' AS [Type], '' AS [Name] FROM dbo.[Release] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'ServerTask' AS [Type], [Name] FROM dbo.[ServerTask] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Subscription' AS [Type], [Name] FROM dbo.[Subscription] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'TagSet' AS [Type], [Name] FROM dbo.[TagSet] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Team' AS [Type], [Name] FROM dbo.[Team] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Tenant' AS [Type], [Name] FROM dbo.[Tenant] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'User, UserWithExternalId' AS [Type], [Username] AS [Name] FROM dbo.[User] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], null as [SpaceId], 'UserRole' AS [Type], [Name] FROM dbo.[UserRole] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'VariableSet' AS [Type], '' AS [Name] FROM dbo.[VariableSet] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'Worker' AS [Type], [Name] FROM dbo.[Worker] WITH (NOLOCK)
  UNION ALL
  SELECT [Id], [SpaceId], 'WorkerPool' AS [Type], [Name] FROM dbo.[WorkerPool] WITH (NOLOCK)