
CREATE TABLE dbo.Employees 
(
	Id int NOT NULL,
	FirstName varchar(200) NOT NULL,
	LastName varchar(200) NOT NULL
);

GO

INSERT into dbo.Employees VALUES (1, 'Anthony', 'Smith');
INSERT into dbo.Employees VALUES (2, 'John', 'Smith');
INSERT into dbo.Employees VALUES (3, 'Mike', 'Smith');